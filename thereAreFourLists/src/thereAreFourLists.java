
/* thereAreFourLists.java
 * Author: Charles Semple - Date Created: July 11, 2012 - Date Completed: July 12, 2012 
 * 
 * In under two hours: Write an application that reads in four provided text files full of words on their own lines
 * on individual threads for each file, and then merge and sort all of the words together, and output a file that has
 * the new sorted list. 
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class thereAreFourLists {
	
	static int NUMBEROFFILES = 4;                //Number of files that are going to be opened.
	
	public static void main(String[] args){;

		String[] filePaths = new String[NUMBEROFFILES];              //Array of file paths. Filled below
		fileOpenThread[] threads = new fileOpenThread[4];            //Array of fileOpenThread objects to run.
		ArrayList<String> totalList = new ArrayList<String>();       //ArrayList to be filled with the whole list of words
		
		String listDirectory = "C:\\tempcode\\sample-word-list\\";   //Directory where the lists live. 
		
		//Allow for user to input a directory, if the console exists
		if(System.console() != null){
			listDirectory = System.console().readLine("Where are the word lists?:");
		}
		
		//Insert paths as strings into array for use in loop below.
		filePaths[0] = listDirectory + "list1.txt";  
		filePaths[1] = listDirectory + "list2.txt";
		filePaths[2] = listDirectory + "list3.txt";
		filePaths[3] = listDirectory + "list4.txt";

		
		//Loop to create threads for each file, and start running them.
		for(int i = 0; i < 4; i++){
			threads[i] = new fileOpenThread(filePaths[i]);
			threads[i].start();
		}
		
		//Loop to join threads after they are done executing. - Will throw an exception if joining fails.
		//Also add the lists to the large, final list as they come in.
		for(int i = 0; i < 4; i++){
			try{
				threads[i].join();
				totalList.addAll(threads[i].wordList);
			} catch (InterruptedException e) {
				System.out.print("ERROR: Join failed");
			}
		}
		
		//Sort the ArrayList with all of the words in it (No need to re-invent the wheel).
		Collections.sort(totalList, String.CASE_INSENSITIVE_ORDER);    //Case-insensitive just in case.
		
		//Output sorted ArrayList to output.txt, to be stashed in same folder as input files.
		try {
			File outFile = new File(listDirectory + "output.txt");
			
			//If output.txt exists, overwrite to replace it.
			if (outFile.exists()) {
				outFile.delete();
			}
			//Create a new output.txt
			outFile.createNewFile();
			
			FileWriter fw = new FileWriter(outFile.getAbsoluteFile());             //FileWriter object targetted to \\output.txt
			BufferedWriter bw = new BufferedWriter(fw);                            //Writer to, well, write lines.
			
			/*Loop to print the final sorted list to the output file.
			*Each iteration prints the next word, and, if it's not
			*the last iteration, a newline character               */
			for(int i = 0; i < totalList.size(); i++){
				bw.write(totalList.get(i));
				if(i != (totalList.size())-1){
					bw.write("\n");
				}
			}
			//Close the file. We're all done here.
			bw.close(); 
			
		} catch (Exception e) {                       //~Exception: Failure to open an output.txt file
			// TODO Auto-generated catch block
			System.out.print("ERROR: Cannot output file.");
		}
		
	}
}

/*fileOpenThread class:
 * Contains a string for the input file's path and name and an ArrayList
 * to populate with the input file's words - Both are public for simplicity's sake.
 * 
 * Methods added: 
 * fileOpenThread(String fileName) - Constructor to open a file with a given fileName
 * 
 */
class fileOpenThread extends Thread {
	String fileToRead;                        //String for filename to read
	ArrayList<String> wordList;               //ArrayList for words read in from the file
	
	//Function called when thread starts.
	public void run(){
		
		//If we have nothing to read, we don't have anything to do here.
		if(fileToRead == null){
			System.out.print("ERROR: No file provided for " + getName());
			return;
		}
		
		try {
			FileInputStream fstream = new FileInputStream(fileToRead);                //InputStream set to read an input file
			DataInputStream in = new DataInputStream(fstream);                        //Datainput from file input
			BufferedReader br = new BufferedReader(new InputStreamReader(in));        //Reader to, well, read lines.
			String tempString;                                                     //String to compare for emptiness and to hold lines.
			wordList = new ArrayList<String>();
			
			//Loop: Read in each line, make sure it's not NULL - If not, add it to the ArrayList
			while((tempString = br.readLine()) != null){
				wordList.add(tempString);
			}
			
			//Close the file. All done.
			in.close();
		} catch (Exception e) {     //~Exception: File failed to open
			System.out.println("Failed to open file:" + fileToRead);
		}
		
	}	
	
	//Constructor to set a file name to read in.
	public fileOpenThread(String fileName){
		fileToRead = fileName;
	}
		
}

